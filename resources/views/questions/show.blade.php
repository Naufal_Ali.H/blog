@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ $question->title }} </h4>
        <p> {{ $question->body }} </p>
    </div>
@endsection