<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Register</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="welcome" method="POST">
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="fname">
            <br><br>
            <label>Last name:</label><br><br>
            <input type="text" name="lname">
            <br><br>
            <label>Gender:</label><br><br>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other <br>
            <br>
            <label>Nationality:</label><br><br>
            <select name="Nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporean">Singaporean</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>
            <br><br>
            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox">Bahasa Indonesia <br>
            <input type="checkbox">English <br>
            <input type="checkbox">Other <br>
            <br>
            <label>Bio:</label><br><br>
            <textarea cols="33" rows="15"></textarea>
            <br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>